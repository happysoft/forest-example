package com.dtflys.forest.example.client;

import com.dtflys.forest.annotation.Get;

public interface Cn12306 {

    @Get(url = "${idServiceUrl}")
    String index();
}
